import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Locators {

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "/Users/rarustei/Documents/AutomationProgram/selenium/SeleniumProject/chromedriver");
//        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--disable-notifications");

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
        Thread.sleep(3000);

//       // WebElement username = driver.findElement(By.cssSelector(".oxd-text.oxd-text--h5.orangehrm-login-title"));
//        System.out.println(driver.findElement(By.cssSelector(".oxd-text.oxd-text--h5.orangehrm-login-title")).getText());
//        System.out.println("Elements by ID " + driver.findElement(By.id("app")).getText());
//        System.out.println(driver.findElement(By.xpath("//div[@class='orangehrm-login-footer']")).getText());
//        System.out.println(driver.findElement(By.className("oxd-form-row")).getText());
//        String result = driver.findElement(By.xpath("//div[@class='orangehrm-login-footer']")).getText();
//        System.out.println(" The Result is " + result);
//        System.out.println(driver.getClass());
//
//        boolean result2 = driver.getPageSource().contains("String to find");
//        System.out.println(driver.getPageSource());
//        System.out.println(result2);
//        System.out.println(driver.getTitle());
//        System.out.println(driver.getCurrentUrl());

//        System.out.println(driver.findElement(By.id("app")).getText());
//
//        driver.findElement(By.id("findID")).getAttribute("value");

//        //to handle multiple windows
//        private String winHandleBefore;
//        winHandleBefore = driver.getWindowHandle();
//        driver.switchTo().window(winHandleBefore);

        driver.findElement(By.name("username")).sendKeys("Admin");
        driver.findElement(By.name("password")).sendKeys("admin123");
        driver.findElement(By.cssSelector("button[type='submit']")).click();
        Thread.sleep(3000);

        driver.findElement(By.cssSelector("body > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > aside:nth-child(1) > nav:nth-child(1) > div:nth-child(2) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1) > span:nth-child(2)")).click();
        Thread.sleep(3000);

        boolean textBox = driver.findElement(By.cssSelector("input[placeholder='Type for hints...']")).isEnabled();
        System.out.println(textBox);
        driver.findElement(By.xpath("(//i[@class='oxd-icon bi-check oxd-checkbox-input-icon'])[1]"));
        Thread.sleep(3000);
        System.out.println(driver.findElement(By.xpath("(//i[@class='oxd-icon bi-check oxd-checkbox-input-icon'])[1]")).isSelected());

        List<WebElement> rowsNumber = driver.findElements(By.xpath("//body[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]"));
        int rowCount = rowsNumber.size();
        System.out.println("No of rows in this table : " + rowCount);

        List<WebElement> columnsNumber = driver.findElements(By.xpath("//body[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]"));
        int columnCount = columnsNumber.size();
        System.out.println("No of columns in this table : " + columnCount);

//        WebElement cellAddress = RowTable.findElement(By.xpath("//body[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]"));
//                String value = cellAddress.getText();
//        System.out.println("The Cell Value is : " +value);

        driver.quit();

    }
}
